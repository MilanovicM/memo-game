import { CardImage } from "../entities/CardImage";
import { generateID } from "../shared/utilities";

const preloadImages = images => {
   images.forEach(imageUrl => {
        const i = new Image();
        i.src = imageUrl;
    });
};

const setupImages = (images, n=6) => {
    const initialImages = images.slice(0, n).map(imageURL => createImage(imageURL));
    const allImages = [...initialImages, ...initialImages]
        .map(image => {
            const id = generateID();
            const { url, groupId, isOpened } = image;
            return createImage(url, groupId, isOpened, id);
        });

    return shuffleArray(allImages);
};

const shuffleArray = elements => {
    return elements
        .map(element => ({ element, value: generateID() }))
        .sort((a, b) => a.value - b.value)
        .map(element => element.element);
};

const createImage = (url, groupId, isOpened, id) => new CardImage(url, groupId, isOpened, id);


export { setupImages, createImage, preloadImages };