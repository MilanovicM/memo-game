import React, { Fragment, Component } from 'react';
import { Header } from '../../components/Header/Header';
import { Reset } from '../../components/Reset/Reset';
import { CardGrid } from '../../components/CardGrid/CardGrid';
import { Timer } from '../../components/Timer/Timer';
import { setupImages, createImage, preloadImages } from '../../services/shuffleService';
import { images } from "../../shared/images";
import { GameStateManager } from '../../entities/GameStateManager';
import { Score } from '../../components/Score/Score';

import './Main.css';

class Main extends Component {
  constructor(props) {
    super(props);

    this.state = {
      imagesList: [],
      runningTimer: false,
      gameOver: false,
      isScoreReady: false
    };

    this.active = [];
    this.timeout = null;
    this.levelChosen = null;

    this.onCardOpen = this.onCardOpen.bind(this);
    this.gameOverTime = this.gameOverTime.bind(this);
    this.onReset = this.onReset.bind(this);
  };

  componentDidMount() {
    preloadImages(images[this.props.selectedTheme]);
    this.levelChosen = new GameStateManager(this.props.numOfPairs);
    const theme = this.props.selectedTheme;
    this.setState({ imagesList: setupImages(images[theme], this.props.numOfPairs) });
  };


  onCardOpen(image) {
    (!this.state.runningTimer) && this.setState({ runningTimer: true });

    const { imagesList } = this.state;

    let images = [...imagesList];

    if (this.active.length === 2) {
      clearTimeout(this.timeout);
      images = this.closeImages(images, this.active);
      this.emptyActiveImages();
    };

    images = this.openImage(images, image);
    this.updateImageList(images);
    this.addToActive(image);

    if (this.active.length === 1) {
      this.levelChosen.incNumberOfMovesPlayed();
      return;
    };

    if (this.isMatchingPair()) {
      this.levelChosen.incNumberOfMatchesFound();

      this.levelChosen.calculateMovesBonus();

      if (this.levelChosen.isGameOver()) {

        this.setState(() => ({ isScoreReady: true }), () => {
          this.setState({ gameOver: true, runningTimer: false });
        })
      }

      return this.emptyActiveImages();
    };

    const updatedImagesList = this.closeImages(images, this.active);

    this.timeout = setTimeout(() => {
      this.emptyActiveImages();
      this.updateImageList(updatedImagesList);
    }, 1500);
    this.levelChosen.calculateMovesBonus();
  };

  updateImageList(images) {
    this.setState({ imagesList: images });
  };

  emptyActiveImages() {
    this.active = [];
  };

  addToActive(image) {
    this.active.push(image);
  };

  openImage(images, image) {
    const { url, groupId, id } = image;
    const uptImage = createImage(url, groupId, true, id);

    const index = images.findIndex(elem => elem.id === image.id);
    images.splice(index, 1, uptImage);

    return images;
  };

  closeImages(images, imagesToClose) {
    return images.map(image => {
      const { url, groupId, id } = image;

      return (
        imagesToClose.findIndex(img => img.id === image.id) !== -1
          ? createImage(url, groupId, false, id)
          : image
      );
    });
  };

  isMatchingPair() {
    return this.active[0].groupId === this.active[1].groupId;
  };

  gameOverTime(time) {
    this.levelChosen.calculateTimeBonus(time);
    this.levelChosen.getGameScore();
  };

  onReset() {
    this.levelChosen.onGameOver();
    this.levelChosen = new GameStateManager(this.props.numOfPairs);
    this.setState({
      imagesList: setupImages(images[this.props.selectedTheme], this.props.numOfPairs),
      gameOver: false,
      runningTimer: false,
      isScoreReady: false
    });
  };

  render() {
    const { imagesList, gameOver, runningTimer, isScoreReady } = this.state;

    return (
      <Fragment>
        <Header selectedLevel={this.props.numOfPairs}><Reset onReset={this.onReset} /></Header>
        <div className="main">
          <CardGrid
            images={imagesList}
            onCardOpen={this.onCardOpen}
            selectedTheme={this.props.selectedTheme}
            selectedLevel={this.props.numOfPairs}
          />
          <div className="score">
            <Timer
              runningTimer={runningTimer}
              isScoreReady={isScoreReady}
              gameOver={gameOver}
              gameOverTime={this.gameOverTime}
            />
            {isScoreReady && <Score score={this.levelChosen.score} />}
          </div>
        </div>
      </Fragment>
    );
  };
};

export { Main };
