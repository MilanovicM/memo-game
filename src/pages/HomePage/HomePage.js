import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Header } from '../../components/Header/Header';
import { CardThemes } from '../../components/CardThemes/CardThemes';
import { Levels } from '../../components/Levels/Levels';
import { StartGameButton } from '../../components/StartGameButton/StartGameButton';

import './HomePage.css'

const HomePage = ({ onChooseTheme, onChooseLevel, currentLevel, currentTheme }) => {

    return (
        <Fragment>
            <Header />
            <div className="home-page">
                <Levels onChooseLevel={onChooseLevel} currentLevel={currentLevel} />
                <CardThemes onChooseTheme={onChooseTheme} currentTheme={currentTheme} />
                <StartGameButton/>
            </div>
        </Fragment>
    )
}

HomePage.propTypes = {
    onChooseTheme: PropTypes.func.isRequired,
    onChooseLevel: PropTypes.func.isRequired,
    currentLevel: PropTypes.number.isRequired,
    currentTheme: PropTypes.string.isRequired
}

export { HomePage }