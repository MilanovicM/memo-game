import React from "react";
import PropTypes from 'prop-types';
import { Card } from "../Card/Card";
import "./CardGrid.css";

const CardGrid = ({ images, onCardOpen, selectedTheme, selectedLevel }) => {
    const className = selectedLevel;

    return (
        <div className={`card-grid level--${className}`}>
            {images.map((image, i) => <Card key={i} image={image} onCardOpen={onCardOpen} selectedTheme={selectedTheme} />)}
        </div>
    );
}

CardGrid.propTypes = {
    images: PropTypes.array.isRequired,
    onCardOpen: PropTypes.func.isRequired,
    selectedTheme: PropTypes.string.isRequired,
    selectedLevel: PropTypes.number.isRequired
}

export { CardGrid };