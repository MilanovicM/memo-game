import React from 'react';
import { MemoryRouter } from 'react-router-dom'
import { Header } from './Header';
import renderer from 'react-test-renderer';

test('should Header render correctly', () => {
    const header = renderer.create(
        <MemoryRouter>
            <Header children={<div>Hello World!</div>}/>
        </MemoryRouter>
    ).toJSON();
    expect(header).toMatchSnapshot();
});