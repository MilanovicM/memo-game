import React from "react";
import { Card } from "./Card";
import renderer from "react-test-renderer";
import { CardImage } from "../../entities/CardImage";

const image = new CardImage("http://pngimg.com/uploads/winnie_pooh/winnie_pooh_PNG37565.png");

test("Card should be opened", () => {
    const card = renderer.create(<Card isOpened={true} image={image} />).toJSON();
    expect(card).toMatchSnapshot();
});

test("Card should be closed", () => {
    const card = renderer.create(<Card isOpened={false} image={image} />).toJSON();
    expect(card).toMatchSnapshot();
});