import React from 'react';
import { Reset } from './Reset';
import renderer from 'react-test-renderer';

test("should Reset render correctly", () => {
    const reset = renderer.create(<Reset onReset={()=>'Hello World!'} />).toJSON();
    expect(reset).toMatchSnapshot();
});