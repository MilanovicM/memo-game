import React from 'react';
import './Reset.css';
import PropTypes from "prop-types";

const Reset = ({ onReset }) => (
    <i className="material-icons reset-button" onClick={onReset}>refresh</i>
);

Reset.propTypes = {
    onReset: PropTypes.func.isRequired
};

export { Reset };