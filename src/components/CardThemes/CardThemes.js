import React from 'react';
import PropTypes from 'prop-types';
import { cardDeck } from '../../shared/images';

import './CardThemes.css'

const CardThemes = ({ onChooseTheme, currentTheme }) => {
    const activeClass = 'current-theme';

    return (
    <div className="themes">
        <div
            className={`card-theme ${currentTheme === 'toons' ? activeClass: ''}`}
            onClick={onChooseTheme}  
        >
            <img src={cardDeck["toons"]} alt="deck" id="toons"/>
        </div>
        <div
            className={`card-theme ${currentTheme === 'marvel' ? activeClass: ''}`}
            onClick={onChooseTheme}
        >
            <img src={cardDeck["marvel"]} alt="deck" id="marvel"/>
        </div>
        <div
            className={`card-theme ${currentTheme === 'dev' ? activeClass: ''}`}
            onClick={onChooseTheme}
        >
            <img src={cardDeck["dev"]} alt="deck" id="dev"/>
        </div>
        <div
            className={`card-theme ${currentTheme === 'anime' ? activeClass: ''}`}
            onClick={onChooseTheme}
        >
            <img src={cardDeck["anime"]} alt="deck" id="anime"/>
        </div>
    </div>
)}

CardThemes.propTypes = {
    onChooseTheme: PropTypes.func.isRequired,
    currentTheme: PropTypes.string.isRequired
}

export { CardThemes }